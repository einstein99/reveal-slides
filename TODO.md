Nummer auf Folien

Mehr Übungsaufgaben auf: https://learngitbranching.js.org/

* Title:
    - Name                                              -
    - GK Logo                                           Frieder
    - Git Logo                                          -
* Outline slide
    - (Ablaufplan)                                      -
* Intro Slides
    - Motivation                                        Frieder
* Git Geschichte
    - Linus Torvald (Linux)                             Kevin
* Anwendsfälle
    - Bsp:    
* Generelle Funktionsweise
    - VCS
    - Was ist ein Commit?
        - Welche infos stehen drin?
        - wie erstelle ich einen?
* Konfiguration
    - user                                              Kevin
    - mail                                              Kevin
    - gitignore (lokal, global)                         Kevin
    - remotes (add, configure)                          Kevin
    - (editor)                                          Kevin
    - ssh key ins gitlab                                Kevin
* Übungen
    - wie zeige ich den aktuellen Status an?            Kai 
    - wie zeige ich mir die lokalen branches an?        Kai
    - wie zeige ich die remote branches an?             Kai
    - wie wechsle ich lokal einen Branch                Kai
    - wie pushe ich meinen Branch richtig?              Kai
    - Wie zeige ich die Diffs beim commit an?           Kai
    - Wie merge ich richtige?                           Kai
    - Wie entferne ich einen Branch                     Kai
* Panic resolution
    - detached head - wie komm ich da wieder raus       Frieder
    - stash current changes,                            Frieder
    - falls alles fehl schlägt:                         -
      - copy changed files                              -
      - rm -rf project/ && git clone again              -
      - copy changes back                               -
* Gitlab-Flow
    - master = prod                                     Frieder
    - dev = development/staging                         Frieder
    - feature-branches                                  Frieder
    - issues                                            Frieder
    - merge/pull-requests                               Frieder
	  - erstellen, betrachten, schließen                -
* Generelle Hinweise für Kollaboration                  _Frieder_
    - lest die Docs, intensiv
    - schaut in bestehende Issues
    - auch in schon geschlossene
    - lest den Source Code, intensiv
    - macht euch mit dem Programmierstil vertraut
